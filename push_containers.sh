#!/usr/bin/env zsh

source names.sh

docker push nobbz/wordcount:latest

for base in $bases; do
    docker push nobbz/${base}:latest
done
