defmodule Wordcount do
  @moduledoc """
  Documentation for Wordcount.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Wordcount.hello
      :world

  """
  def main(input_file) do
    input_file
    |> File.open!([:read, :utf8])
    |> IO.stream(:line)
    |> Stream.map(&String.trim/1)
    |> Stream.flat_map(&String.split/1)
    |> Stream.map(&trim_non_letter/1)
    |> Stream.map(&String.downcase/1)
    |> Enum.reduce(%{}, &count/2)
    |> Enum.sort(&sorter/2)
    |> Enum.map(&print/1)
  end

  defp sorter({_, c1}, {_, c2}) when c1 > c2, do: true
  defp sorter({w1, c}, {w2, c}) when w1 < w2, do: true
  defp sorter(_, _), do: false

  defp print({word, count}), do: IO.puts("#{count}\t#{word}")

  defp count("", acc), do: acc
  defp count(word, acc) do
    Map.update(acc, word, 1, &(&1 + 1))
  end

  defp trim_non_letter(str) do
    str
    |> trim_left()
    |> trim_right()
  end

  defp trim_left(<<>>), do: <<>>
  defp trim_left(word = <<c::utf8, tail::binary>>) do
    if letter?(c), do: word, else: trim_left(tail)
  end

  defp trim_right(word), do: word |> String.reverse() |> trim_left() |> String.reverse()

  defp letter?(cp) when is_integer(cp), do: letter?(<<cp::utf8>>)
  defp letter?(cp) when is_binary(cp), do: Regex.match?(~r"^\p{L}$"u, cp)
end
