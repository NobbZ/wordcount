#!/usr/bin/env zsh

set -e

source names.sh

function build_base() {
    echo "Building the global base image"
    docker build -t nobbz/wordcount:latest __wordcount_base
}

function build_bases() {
    for base in $bases; do
        echo "Building base image for ${base} runtime/compiler"
        docker build -t nobbz/${base}:latest _${base}_base
    done
}

function build_impls() {
    for impl in $implementations; do
        echo "Building runtime image for ${impl}"
        docker build -t nobbz/impl_${impl}:latest $impl
    done
}

case $1 in
    base)
        build_base
        ;;
    bases)
        build_base
        build_bases
        ;;
    impls)
        build_base
        build_bases
        build_impls
        ;;
    impls-only)
        build_impls
        ;;
esac
