package pair

import "sort"

type Pairs []Pair

type Pair struct {
	Count uint64
	Word  string
}

var _ sort.Interface = Pairs{}

func (ps Pairs) Len() int {
	return len(ps)
}

func (ps Pairs) Less(i, j int) bool {
	if ps[i].Count > ps[j].Count {
		return true
	} else if ps[i].Count == ps[j].Count && ps[i].Word < ps[j].Word {
		return true
	}
	return false
}

func (ps Pairs) Swap(i, j int) {
	ps[i], ps[j] = ps[j], ps[i]
}
