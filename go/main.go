package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strings"
	"unicode"

	"wordcount/pair"
)

func main() {
	fileName := os.Args[1]

	f, err := os.Open(fileName)
	if err != nil {
		fmt.Printf("File '%s' error: %v", fileName, err)
		os.Exit(1)
	}

	histogram, err := histoFromFile(f)
	if err != nil && err != io.EOF {
		log.Fatal(err)
	}

	resultPairs := histogramToPairs(histogram)

	sort.Sort(resultPairs)

	for _, thePair := range resultPairs {
		fmt.Printf("%d\t%s\n", thePair.Count, thePair.Word)
	}
}

func histoFromFile(f *os.File) (map[string]uint64, error) {
	var err error

	histogram := make(map[string]uint64)
	r := bufio.NewReader(f)

	for err == nil {
		var line string

		line, err = readLine(r)

		processLine(line, histogram)
	}

	return histogram, err
}

func readLine(r *bufio.Reader) (string, error) {
	var (
		err    error
		prefix bool
	)

	shortBuffer := make([]byte, 0)
	growBuffer := make([]byte, 0)

	for {
		shortBuffer, prefix, err = r.ReadLine()
		if err != nil && err != io.EOF {
			log.Fatal(err)
		}
		growBuffer = append(growBuffer, shortBuffer...)
		if !prefix {
			break
		}
	}

	return string(growBuffer), err
}

func processLine(line string, histogram map[string]uint64) {
	for _, word := range strings.Split(line, " ") {
		word = strings.TrimFunc(word, func(r rune) bool { return !unicode.IsLetter(r) })
		if len(word) != 0 {
			histogram[strings.ToLower(word)]++
		}
	}
}

func histogramToPairs(histogram map[string]uint64) pair.Pairs {
	result := make(pair.Pairs, len(histogram))
	index := 0

	for word, count := range histogram {
		result[index] = pair.Pair{Count: count, Word: word}
		index++
	}

	return result
}
