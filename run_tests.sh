#!/usr/bin/env zsh

source names.sh

fails=()

for lang in $implementations; do
    echo "Running tests for ${lang}"
    if docker run --entrypoint="/bin/test.sh" nobbz/impl_${lang}:latest; then :;
    else
        fails+=(${lang})
    fi
done

for fail in $fails; do
    echo " * ${fail}"
done