#!/usr/bin/env bash

set -e

input_files=$(find ${WC_PATH} -name '*.in' | sed "s|${WC_PATH}/||" | sort -n -t "_" -k 1,1)

for file in $input_files; do
    echo Testing ${file/.in/}
    ${BIN_PATH}/${SCRIPTNAME} ${WC_PATH}/${file} | diff ${WC_PATH}/${file/.in/.out} -
done
